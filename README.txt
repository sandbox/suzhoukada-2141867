Demo Code:
<?php
  $options = array(
    // URL of the site.
    'p2i_url' => 'http://drupal.org',
    // Size of the generated screenshot.
    'p2i_size' => '480x400',
  );

  // Return html image.
  echo theme_page2images_image($options);
  // Return image url as string.
  echo page2images_get_thumbnail($options);
