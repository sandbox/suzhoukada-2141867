<?php

/**
 * @file
 * Administration page callbacks for the page2images module.
 */

/**
 * Form builder. Configure page2images.
 *
 * @ingroup forms
 */
function page2images_admin_settings() {
  $form = array();

  // Page2images information.
  $form['page2images_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page2Images Information'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['page2images_info']['page2images_get_account'] = array(
    '#type' => 'item',
    '#title' => t('Get Account'),
    '#description' => t('You can get your "Key" by signing up for automated screenshots at: <a target="_blank" href="http://www.page2images.com/">Page2Images</a>'),
  );

  // Keys value.
  $form['page2images_keys'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page2Images API keys'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['page2images_keys']['page2images_restful_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Restful Key'),
    '#default_value' => variable_get('page2images_restful_key', ''),
    '#required' => FALSE,
  );

  // Optional value.
  $form['page2images_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page2Images Options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['page2images_options']['page2images_device'] = array(
    '#type' => 'select',
    '#title' => t('Device'),
    '#options' => array(t('iPhone4'), t('iPhone5'), t('Android'),
      t('WinPhone'), t('iPad'), t('Android Pad'), t('Desktop')),
    '#default_value' => variable_get('page2images_device', 6),
    '#required' => TRUE,
    '#description' => t('Screenshot from the selected device.'),
  );

  $form['page2images_options']['page2images_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Size'),
    '#default_value' => variable_get('page2images_size', ''),
    '#required' => FALSE,
    '#description' => t('The screenshot size expected. Such as "800x600", "400x0", "1024x768" etc.'),
  );

  $form['page2images_options']['page2images_screen_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Screen size'),
    '#default_value' => variable_get('page2images_screen_size', ''),
    '#required' => FALSE,
    '#description' => t('The screenshot screen size. Such as "800x600", "1024x768" etc.'),
  );

  $form['page2images_options']['page2images_fullpage'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show full page'),
    '#default_value' => variable_get('page2images_fullpage', FALSE),
    '#required' => FALSE,
    '#description' => t('If check will show full page otherwise only show the screen size.'),
  );

  $form['page2images_options']['page2images_refresh'] = array(
    '#type' => 'checkbox',
    '#title' => t('Refresh'),
    '#default_value' => variable_get('page2images_refresh', FALSE),
    '#required' => FALSE,
    '#description' => t('If you does not want to use the cache, please set this value to true.'),
  );

  $form['page2images_options']['page2images_imageformat'] = array(
    '#type' => 'select',
    '#title' => t('Image format'),
    '#options' => array(
      'jpg' => t('jpg'),
      'png' => t('png'),
    ),
    '#default_value' => variable_get('page2images_imageformat', 'jpg'),
    '#required' => FALSE,
  );

  $form['page2images_options']['page2images_wait'] = array(
    '#type' => 'textfield',
    '#title' => t('Wait'),
    '#default_value' => variable_get('page2images_wait', 0),
    '#required' => FALSE,
    '#description' => t('How many second wait after page load complete.'),
  );

  // Advance optional value.
  $form['page2images_adv_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page2Images Advanced Options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['page2images_adv_options']['page2images_cache_days'] = array(
    '#type' => 'textfield',
    '#title' => t('Cache days'),
    '#default_value' => variable_get('page2images_cache_days', 3),
    '#description' => t('How many days the images are valid in your cache, Enter 0 (zero) to never update screenshots once cached or -1 to disable caching and always use embedded method instead'),
    '#size' => 10,
  );
  $form['page2images_adv_options']['page2images_thumbs_folder'] = array(
    '#type' => 'textfield',
    '#title' => t('Thumbnails folder'),
    '#default_value' => variable_get('page2images_thumbs_folder', 'page2images_thumbnails'),
    '#required' => TRUE,
    '#description' => t('This is a subfolder of the "File system path" folder.'),
  );

  return system_settings_form($form);
}
