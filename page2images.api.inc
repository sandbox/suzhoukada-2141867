<?php
/**
 * @file
 * For the latest documentation and best practices: please visit
 * http://www.page2images.com/api_document/REST_Web_Service_Interface.
 */

define('PAGE2IMAGES_API_URL', 'http://api.page2images.com/restfullink/');
define('PAGE2IMAGES_RESTFUL_KEY', variable_get('page2images_restful_key', ''));

define('PAGE2IMAGES_THUMBNAIL_DIR', file_default_scheme() . '://' . variable_get('page2images_thumbs_folder', '') . '/');
define('PAGE2IMAGES_THUMBNAIL_URI', file_create_url(PAGE2IMAGES_THUMBNAIL_DIR) . '/');
define('PAGE2IMAGES_CACHE_DAYS', variable_get('page2images_cache_days', ''));

error_reporting(E_ALL ^ E_NOTICE);

/**
 * Gets website thumbnail.
 */
function page2images_get_image_url($options_arr = array()) {
  $image_url = FALSE;
  $options_arr = page2images_format_options($options_arr);
  $image_url = page2images_get_image($options_arr);
  return $image_url;
}

/**
 * Gets the thumbnail for the website, stores it in the cache.
 *
 * Returns the relative path of the cached image.
 */
function page2images_get_image($options_arr) {
  page2images_create_cache_directory();
  $image_url = page2images_get_cached_img($options_arr);
  return $image_url;
}

/**
 * Generate options.
 */
function page2images_format_options($options_arr) {
  $options_arr['p2i_url'] = isset($options_arr['p2i_url']) ? $options_arr['p2i_url'] : "";
  $options_arr['p2i_device'] = isset($options_arr['p2i_device']) ? $options_arr['p2i_device'] : variable_get('page2images_device');
  $options_arr['p2i_key'] = variable_get('page2images_restful_key');
  $options_arr['p2i_size'] = isset($options_arr['p2i_size']) ? $options_arr['p2i_size'] : variable_get('page2images_size');
  $options_arr['p2i_screen'] = isset($options_arr['p2i_screen']) ? $options_arr['p2i_screen'] : variable_get('page2images_screen_size');
  $options_arr['p2i_fullpage'] = isset($options_arr['p2i_fullpage']) ? $options_arr['p2i_fullpage'] : variable_get('page2images_fullpage');
  $options_arr['p2i_refresh'] = isset($options_arr['p2i_refresh']) ? $options_arr['p2i_refresh'] : variable_get('page2images_refresh');
  $options_arr['p2i_imageformat'] = isset($options_arr['p2i_imageformat']) ? $options_arr['p2i_imageformat'] : variable_get('page2images_imageformat');
  $options_arr['p2i_wait'] = isset($options_arr['p2i_wait']) ? $options_arr['p2i_wait'] : variable_get('page2images_wait');
  return $options_arr;
}

/**
 * Delete thumbnail what stored in local profile.
 */
function page2images_delete_catch_image($url, $options_arr = array()) {
  $options_arr = page2images_format_options($options_arr);
  $options_arr['p2i_url'] = $url;
  ksort($options_arr);
  $img_filename = md5(http_build_query($options_arr)) . '.' . $options_arr['p2i_imageformat'];
  $img_file = PAGE2IMAGES_THUMBNAIL_DIR . $img_filename;

  if (file_exists($img_file)) {
    @unlink($img_file);
  }
}

/**
 * Get a thumbnail, caching it first if possible.
 */
function page2images_get_cached_img($options_arr = NULL) {
  $options_arr = is_array($options_arr) ? $options_arr : array();

  ksort($options_arr);
  $img_filename = md5(http_build_query($options_arr));
  $img_filename .= '.';
  $img_filename .= $options_arr['p2i_imageformat'];
  $img_file = PAGE2IMAGES_THUMBNAIL_DIR . $img_filename;

  $return_image_src = FALSE;
  // Work out if we need to update the cached thumbnail.
  $is_force_update = $options_arr['p2i_refresh'] ? TRUE : FALSE;

  if (file_exists($img_file)) {
    return PAGE2IMAGES_THUMBNAIL_URI . $img_filename;
  }

  if ($is_force_update || page2images_cache_file($img_file) || !file_exists($img_file)) {
    $response = page2images_get_content(PAGE2IMAGES_API_URL, $options_arr);
    if (!empty($response)) {
      $json_data = json_decode($response);
      if (!empty($json_data->status) && $json_data->status == "finished") {
        page2images_download_image($json_data->image_url, $img_file);
      }
    }
  }

  // Check if file exists.
  if (file_exists($img_file)) {
    $return_image_src = PAGE2IMAGES_THUMBNAIL_URI . $img_filename;
  }
  return $return_image_src;
}

/**
 * Method to get image at the specified remote Url, save it to local path.
 */
function page2images_download_image($url, $img_file) {
  $responce = page2images_get_content($url, array());

  // Only save data if we managed to get the file content.
  if ($responce) {
    $fop_img_file = fopen($img_file, "w+");
    fwrite($fop_img_file, $responce);
    fclose($fop_img_file);
  }
  else {
    // Try to delete file if download failed.
    if (file_exists($img_file)) {
      @unlink($img_file);
    }
    return FALSE;
  }
  return TRUE;
}

/**
 * Create cache directory if it doesnt exist.
 */
function page2images_create_cache_directory() {
  if (!file_exists(PAGE2IMAGES_THUMBNAIL_DIR)) {
    @mkdir(PAGE2IMAGES_THUMBNAIL_DIR, 0777, TRUE);
  }
  else {
    // Try to make the directory writable.
    @chmod(PAGE2IMAGES_THUMBNAIL_DIR, 0777);
  }
}

/**
 * Determine if specified file has expired from the cache.
 */
function page2images_cache_file($img_file) {
  $cache_days = CACHE_DAYS + 0;
  if ($cache_days == 0 && file_exists($img_file)) {
    return FALSE;
  }
  else {
    $cutoff_seconds = time() - (3600 * 24 * $cache_days);
    return (!file_exists($img_file) || filemtime($img_file) <= $cutoff_seconds);
  }
}

/**
 * Gets file content by URL.
 */
function page2images_get_content($url, $options_arr = array()) {
  $options_str = '?';
  foreach ($options_arr as $key => $value) {
    $options_str .= $key . '=' . $value . '&';
  }
  $options_str = substr($options_str, 0, -1);
  $cu_result = '';
  if (function_exists('curl_init')) {
    $cu_obj = curl_init();
    curl_setopt($cu_obj, CURLOPT_URL, $url . $options_str);
    curl_setopt($cu_obj, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($cu_obj, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($cu_obj, CURLOPT_HEADER, 0);
    $cu_result = curl_exec($cu_obj);
    curl_close($cu_obj);
  }
  else {
    $cu_result = @file_get_contents($url . $options_str);
  }
  return $cu_result;
}
